package com.newsapp.categorypage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.newsapp.categorypage.databinding.FragmentCategoryBinding
import com.newsapp.categorypage.view.CategoryPageAdapter
import com.newsapp.common.base.BaseFragment
import com.newsapp.common.base.BaseViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment() {

    private val viewModel: CategoryPageViewModel by viewModel()
    private lateinit var binding: FragmentCategoryBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCategoryBinding.inflate(inflater, container, false)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureRecyclerView()
    }

    override fun getViewModel(): BaseViewModel = viewModel

    private fun configureRecyclerView() {
        val dividerItemDecoration = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)

        binding.fragmentHomeRv.addItemDecoration(dividerItemDecoration)
        binding.fragmentHomeRv.adapter = CategoryPageAdapter(viewModel)
    }
}
