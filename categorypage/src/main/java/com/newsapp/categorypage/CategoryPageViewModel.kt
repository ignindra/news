package com.newsapp.categorypage

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.newsapp.categorypage.domain.GetNewsCategoriesUseCase
import com.newsapp.common.R
import com.newsapp.common.base.BaseViewModel
import com.newsapp.common.helper.Event
import com.newsapp.model.NewsCategory
import com.newsapp.repository.AppDispatchers
import com.newsapp.repository.helper.Resource
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CategoryPageViewModel(
    private val getNewsCategoriesUseCase: GetNewsCategoriesUseCase,
    private val dispatchers: AppDispatchers
) : BaseViewModel() {

    val newsCategoryMediator = MediatorLiveData<Resource<MutableList<NewsCategory>>>()
    private var categories: LiveData<Resource<MutableList<NewsCategory>>> = MutableLiveData()

    init {
        getCategorySources()
    }

    fun newsCategoryClickHandler(newsCategory: NewsCategory) {
        navigate(CategoryPageFragmentDirections
            .actionCategoryFragmentToHomeFragment(newsCategory.categoryName))
    }

    fun refreshListDataHandler() = getCategorySources()

    private fun getCategorySources(forceRefresh: Boolean = false) = viewModelScope.launch(dispatchers.main) {
        newsCategoryMediator.removeSource(categories)

        withContext(dispatchers.io) {
            categories = getNewsCategoriesUseCase(forceRefresh = forceRefresh)
        }

        newsCategoryMediator.addSource(categories) {
            newsCategoryMediator.value = it

            if (it.status == Resource.Status.ERROR) {
                snackBarError.value = Event(R.string.global_error_message)
            }
        }
    }
}
