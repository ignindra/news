package com.newsapp.categorypage.domain

import com.newsapp.repository.NewsRepository

class GetNewsCategoriesUseCase(private val repository: NewsRepository) {

    suspend operator fun invoke(forceRefresh: Boolean) = repository.getCategoriesWithCache(forceRefresh)
}