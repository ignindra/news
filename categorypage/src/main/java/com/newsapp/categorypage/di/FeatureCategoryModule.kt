package com.newsapp.categorypage.di

import com.newsapp.categorypage.CategoryPageViewModel
import com.newsapp.categorypage.domain.GetNewsCategoriesUseCase
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureCategoryModule = module {
    factory { GetNewsCategoriesUseCase(get()) }

    viewModel { CategoryPageViewModel(get(), get()) }
}