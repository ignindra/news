package com.newsapp.categorypage.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.newsapp.categorypage.CategoryPageViewModel
import com.newsapp.categorypage.R
import com.newsapp.model.NewsCategory

class CategoryPageAdapter(
    private val viewModel: CategoryPageViewModel
) : RecyclerView.Adapter<CategoryPageViewHolder>() {

    private val categories: MutableList<NewsCategory> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CategoryPageViewHolder(
        LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_category, parent, false)
    )

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: CategoryPageViewHolder, position: Int) {
        holder.bindTo(categories[position], viewModel)
    }

    fun updateData(items: MutableList<NewsCategory>) {
        val diffCallback = CategoryItemDiffCallback(categories, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        categories.clear()
        categories.addAll(items)

        diffResult.dispatchUpdatesTo(this)
    }
}