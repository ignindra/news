package com.newsapp.categorypage.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.newsapp.categorypage.CategoryPageViewModel
import com.newsapp.categorypage.databinding.ItemCategoryBinding
import com.newsapp.model.NewsCategory

class CategoryPageViewHolder(parent: View): RecyclerView.ViewHolder(parent) {

    private val binding = ItemCategoryBinding.bind(parent)

    fun bindTo(newsCategory: NewsCategory, viewModel: CategoryPageViewModel) {
        binding.newsCategory = newsCategory
        binding.viewmodel = viewModel
    }
}
