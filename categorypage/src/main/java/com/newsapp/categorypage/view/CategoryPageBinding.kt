package com.newsapp.categorypage.view

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.newsapp.model.NewsCategory
import com.newsapp.repository.helper.Resource

object CategoryPageBinding {
    @BindingAdapter("app:showWhenLoading")
    @JvmStatic
    fun <T>showWhenLoading(view: SwipeRefreshLayout, resource: Resource<T>?) {
        if (resource != null) {
            view.isRefreshing = resource.status == Resource.Status.LOADING
        }
    }

    @BindingAdapter("app:items")
    @JvmStatic fun setItems(recyclerView: RecyclerView, resource: Resource<MutableList<NewsCategory>>?) {
        with(recyclerView.adapter as CategoryPageAdapter) {
            resource?.data?.let { updateData(it) }
        }
    }

    @BindingAdapter("app:showWhenEmptyList")
    @JvmStatic fun showMessageErrorWhenEmptyList(view: View, resource: Resource<MutableList<NewsCategory>>?) {
        val isResourceValid = resource != null &&
                resource.status == Resource.Status.ERROR &&
                resource.data?.isEmpty() ?: false

        view.visibility = if (isResourceValid) View.VISIBLE else View.GONE
    }
}
