package com.newsapp.categorypage.view

import androidx.recyclerview.widget.DiffUtil
import com.newsapp.model.NewsCategory

class CategoryItemDiffCallback(
    private val oldList: MutableList<NewsCategory>,
    private val newList: MutableList<NewsCategory>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int)
            = oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id &&
               oldList[oldItemPosition].categoryName == newList[newItemPosition].categoryName
    }
}