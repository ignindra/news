package com.newsapp.di

import com.newsapp.categorypage.di.featureCategoryModule
import com.newsapp.home.di.featureHomeModule
import com.newsapp.local.di.localModule
import com.newsapp.remote.di.createRemoteModule
import com.newsapp.repository.di.repositoryModule

val appComponent = listOf(
    createRemoteModule("https://newsapi.org/"),
    repositoryModule,
    featureHomeModule,
    featureCategoryModule,
    localModule
)