package com.newsapp.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.concurrent.TimeUnit
import java.util.Date

@Entity
data class NewsArticle(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @Embedded(prefix = "srcId_")
    val source: SourceIdentifier,
    val author: String?,
    val title: String?,
    val description: String?,
    val url: String?,
    val urlToImage: String?,
    val publishedAt: String?,
    val content: String?,
    var lastDataRefreshed: Date
) {

    /**
     * This method will calculate and then checking the last time
     * news article data was last fetched from the remote source.
     *
     * Currently it will return true if the last data was fetched more than 5 minutes ago
     */
    fun hasToRefreshData() : Boolean
            = TimeUnit.MILLISECONDS.toMinutes(Date().time - lastDataRefreshed.time) >= 5
}
