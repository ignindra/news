package com.newsapp.model

data class NewsResponse<T>(
    val status: String?,
    val totalResults: Int,
    val articles: MutableList<T>?,
    val sources: MutableList<T>?
)
