package com.newsapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.concurrent.TimeUnit
import java.util.Date

@Entity
data class NewsSource(
    @PrimaryKey
    val id: String,
    val name: String?,
    val description: String?,
    val url: String?,
    val category: String?,
    val language: String?,
    val country: String?,
    var lastDataRefreshed: Date
) {

    /**
     * This method will calculate and then checking the last time
     * news source data was last fetched from the remote source.
     *
     * Currently it will return true if the last data was fetched more than 5 minutes ago
     */
    fun hasToRefreshData() : Boolean
            = TimeUnit.MILLISECONDS.toMinutes(Date().time - lastDataRefreshed.time) >= 5
}
