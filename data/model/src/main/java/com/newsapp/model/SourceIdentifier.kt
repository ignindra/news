package com.newsapp.model

data class SourceIdentifier (
    val id: String?,
    val name: String?
)
