package com.newsapp.repository

import androidx.lifecycle.LiveData
import com.newsapp.local.dao.NewsCategoryDao
import com.newsapp.local.dao.NewsSourceDao
import com.newsapp.model.NewsCategory
import com.newsapp.model.NewsResponse
import com.newsapp.model.NewsSource
import com.newsapp.remote.NewsDatasource
import com.newsapp.repository.helper.Resource
import com.newsapp.repository.helper.ResourceHandler
import java.util.Date

interface NewsRepository {
    suspend fun getNewsSourcesWithCache(forceRefresh: Boolean): LiveData<Resource<MutableList<NewsSource>>>
    suspend fun getCategoriesWithCache(forceRefresh: Boolean): LiveData<Resource<MutableList<NewsCategory>>>
}

class NewsRepositoryImpl(
    private val dataSource: NewsDatasource,
    private val newsSourceDao: NewsSourceDao,
    private val newsCategoryDao: NewsCategoryDao
): NewsRepository {

    override suspend fun getNewsSourcesWithCache(forceRefresh: Boolean): LiveData<Resource<MutableList<NewsSource>>> {
        return object : ResourceHandler<MutableList<NewsSource>, NewsResponse<NewsSource>>() {

            override fun processResponse(response: NewsResponse<NewsSource>): MutableList<NewsSource>?
                    = response.sources

            override suspend fun saveCallResults(items: MutableList<NewsSource>?) {
                if (!items.isNullOrEmpty()) {
                    return newsSourceDao.save(items)
                }
            }

            override fun shouldFetch(data: MutableList<NewsSource>?): Boolean
                    = !data.isNullOrEmpty() || forceRefresh

            override suspend fun loadFromDb(): MutableList<NewsSource>
                    = newsSourceDao.getNewsSources()

            override suspend fun createCallAsync(): NewsResponse<NewsSource>
                    = dataSource.fetchNewsSourceAsync()

        }.build().asLiveData()
    }

    override suspend fun getCategoriesWithCache(forceRefresh: Boolean): LiveData<Resource<MutableList<NewsCategory>>> {
        return object : ResourceHandler<MutableList<NewsCategory>, Unit>() {
            override fun processResponse(response: Unit): MutableList<NewsCategory>? {
                val currentDate = Date()
                val staticCategory = mutableListOf<NewsCategory>()

                staticCategory.add(NewsCategory(0, "business", currentDate))
                staticCategory.add(NewsCategory(1, "entertainment", currentDate))
                staticCategory.add(NewsCategory(2, "general", currentDate))
                staticCategory.add(NewsCategory(3, "health", currentDate))
                staticCategory.add(NewsCategory(4, "science", currentDate))
                staticCategory.add(NewsCategory(5, "sports", currentDate))
                staticCategory.add(NewsCategory(6, "technology", currentDate))

                return staticCategory
            }

            override suspend fun saveCallResults(items: MutableList<NewsCategory>?) {
                if (!items.isNullOrEmpty()) {
                    return newsCategoryDao.save(items)
                }
            }

            override fun shouldFetch(data: MutableList<NewsCategory>?): Boolean = forceRefresh

            override suspend fun loadFromDb(): MutableList<NewsCategory>
                    = newsCategoryDao.getNewsCategories()

            override suspend fun createCallAsync() {
                // do nothing
            }

        }.build().asLiveData()
    }
}
