package com.newsapp.repository.di

import com.newsapp.repository.AppDispatchers
import com.newsapp.repository.NewsRepository
import com.newsapp.repository.NewsRepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val repositoryModule = module {
    factory { AppDispatchers(Dispatchers.Main, Dispatchers.IO) }
    factory { NewsRepositoryImpl(get(), get(), get()) as NewsRepository }
}
