@file:JvmName("RemoteModule")
package com.newsapp.remote.di

import com.newsapp.remote.NewsDatasource
import com.newsapp.remote.NewsService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private fun createKeyInterceptor() = Interceptor { chain ->
    var request = chain.request()
    val url = request.url().newBuilder().addQueryParameter("apiKey", "1929f9808d294c5a837bb7b01ed385df").build()

    request = request.newBuilder().url(url).build()
    chain.proceed(request)
}

fun createRemoteModule(baseUrl: String) = module {
    factory {
        OkHttpClient
            .Builder()
            .addInterceptor(createKeyInterceptor())
            .build()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    factory { get<Retrofit>().create(NewsService::class.java) }

    factory { NewsDatasource(get()) }
}
