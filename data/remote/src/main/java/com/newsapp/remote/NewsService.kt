package com.newsapp.remote

import com.newsapp.model.NewsResponse
import com.newsapp.model.NewsSource
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {

    @GET("v2/sources")
    suspend fun fetchNewsSourceAsync(
        @Query("category") category: String = "general",
        @Query("q") keyword: String = ""
    ): NewsResponse<NewsSource>

    // TODO: fetch news article asynchronously
}
