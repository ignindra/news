package com.newsapp.remote

class NewsDatasource(private val service: NewsService) {

    suspend fun fetchNewsSourceAsync() = service.fetchNewsSourceAsync()
}
