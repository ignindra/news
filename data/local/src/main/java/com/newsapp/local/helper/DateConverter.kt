package com.newsapp.local.helper

import androidx.room.TypeConverter
import java.util.*

class DateConverter {
    @TypeConverter
    fun fromTimestamp(timestamp: Long?): Date? {
        return if (timestamp != null ) Date(timestamp) else null
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}
