@file:JvmName("DatabaseMetadata")
package com.newsapp.local.helper

internal const val DATABASE_VERSION = 2
internal const val DATABASE_QUALIFIER = "DATABASE"