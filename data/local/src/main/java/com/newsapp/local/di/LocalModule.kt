package com.newsapp.local.di

import com.newsapp.local.NewsAppDatabase
import com.newsapp.local.helper.DATABASE_QUALIFIER
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

val localModule = module {
    single(named(DATABASE_QUALIFIER)) { NewsAppDatabase.buildDatabase(androidContext()) }
    factory { (get(named(DATABASE_QUALIFIER)) as NewsAppDatabase).newsSourceDao() }
    factory { (get(named(DATABASE_QUALIFIER)) as NewsAppDatabase).newsCategoryDao() }
}