package com.newsapp.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.newsapp.model.NewsSource
import java.util.Date

@Dao
abstract class NewsSourceDao: BaseDao<NewsSource>() {

    @Query("SELECT * FROM NewsSource ORDER BY name ASC LIMIT 30")
    abstract suspend fun getNewsSources(): MutableList<NewsSource>

    @Query("SELECT * FROM NewsSource WHERE name = :newsSourceName LIMIT 1")
    abstract suspend fun getNewsSource(newsSourceName: String): NewsSource

    suspend fun save(newsSource: NewsSource) {
        insert(newsSource.apply { lastDataRefreshed = Date() })
    }

    suspend fun save(newsSources: MutableList<NewsSource>) {
        insert(newsSources.apply { forEach { newsSource ->
            newsSource.lastDataRefreshed = Date()
        }})
    }
}
