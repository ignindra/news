package com.newsapp.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.newsapp.model.NewsCategory
import java.util.Date

@Dao
abstract class NewsCategoryDao: BaseDao<NewsCategory>() {

    @Query("SELECT * FROM NewsCategory LIMIT 30")
    abstract suspend fun getNewsCategories(): MutableList<NewsCategory>

    suspend fun save(newsCategories: MutableList<NewsCategory>) {
        insert(newsCategories.apply { forEach { newsCategory ->
            newsCategory.lastDataRefreshed = Date()
        }})
    }
}