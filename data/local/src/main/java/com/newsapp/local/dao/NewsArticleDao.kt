package com.newsapp.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.newsapp.model.NewsArticle
import java.util.Date

@Dao
abstract class NewsArticleDao: BaseDao<NewsArticle>() {

    @Query("SELECT * FROM NewsArticle LIMIT 30")
    abstract suspend fun getNewsArticles(): MutableList<NewsArticle>

    suspend fun save(newsArticles: MutableList<NewsArticle>) {
        insert(newsArticles.apply { forEach { newsArticle ->
            newsArticle.lastDataRefreshed = Date()
        }})
    }
}
