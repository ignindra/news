package com.newsapp.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.newsapp.local.dao.NewsArticleDao
import com.newsapp.local.dao.NewsCategoryDao
import com.newsapp.local.dao.NewsSourceDao
import com.newsapp.local.helper.DATABASE_VERSION
import com.newsapp.local.helper.DateConverter
import com.newsapp.model.NewsArticle
import com.newsapp.model.NewsCategory
import com.newsapp.model.NewsSource

@Database(
    entities = [NewsSource::class, NewsArticle::class, NewsCategory::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class NewsAppDatabase : RoomDatabase() {

    abstract fun newsSourceDao(): NewsSourceDao
    abstract fun newsArticleDao(): NewsArticleDao
    abstract fun newsCategoryDao(): NewsCategoryDao

    companion object {
        fun buildDatabase(ctx: Context): RoomDatabase {
            return Room
                .databaseBuilder(ctx.applicationContext, NewsAppDatabase::class.java, "NewsApp.db")
                .build()
        }
    }
}
