package com.newsapp.local.dao

import androidx.room.Insert
import androidx.room.OnConflictStrategy

abstract class BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    internal abstract suspend fun insert(listData: MutableList<T>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    internal abstract suspend fun insert(data: T)
}