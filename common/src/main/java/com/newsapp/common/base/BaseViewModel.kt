package com.newsapp.common.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.newsapp.common.helper.Event
import com.newsapp.navigation.NavCommand

abstract class BaseViewModel: ViewModel() {

    val snackBarError: MutableLiveData<Event<Int>> = MutableLiveData()

    val navigation: MutableLiveData<Event<NavCommand>> = MutableLiveData()

    fun navigate(directions: NavDirections) {
        navigation.value = Event(NavCommand.To(directions))
    }
}
