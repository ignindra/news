package com.newsapp.common.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.newsapp.common.helper.Event
import com.newsapp.navigation.NavCommand

abstract class BaseFragment: Fragment() {

    open fun getExtras(): FragmentNavigator.Extras = FragmentNavigatorExtras()
    abstract fun getViewModel(): BaseViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observeNavigation(getViewModel())
        setupSnackbar(getViewModel().snackBarError)
    }

    private fun observeNavigation(viewModel: BaseViewModel) {
        viewModel.navigation.observe(viewLifecycleOwner, Observer { event ->
            event?.getContentIfNotHandled()?.run {
                when (this) {
                    is NavCommand.To -> findNavController().navigate(directions, getExtras())
                    is NavCommand.Back -> findNavController().navigateUp()
                }
            }
        })
    }

    private fun setupSnackbar(snackbarEvent: LiveData<Event<Int>>) {
        snackbarEvent.observe(viewLifecycleOwner, Observer { event ->
            event?.getContentIfNotHandled()?.let { res ->
                activity?.run {
                    Snackbar.make(
                        findViewById<View>(android.R.id.content),
                        getString(res),
                        Snackbar.LENGTH_LONG
                    )
                }
            }
        })
    }
}
