package com.newsapp.home.di

import com.newsapp.home.HomeViewModel
import com.newsapp.home.domain.GetNewsSourcesUseCase
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureHomeModule = module {
    factory { GetNewsSourcesUseCase(get()) }

    viewModel { HomeViewModel(get(), get()) }
}
