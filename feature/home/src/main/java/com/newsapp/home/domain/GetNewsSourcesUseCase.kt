package com.newsapp.home.domain

import com.newsapp.repository.NewsRepository

class GetNewsSourcesUseCase(private val repository: NewsRepository) {

    suspend operator fun invoke(forceRefresh: Boolean) = repository.getNewsSourcesWithCache(forceRefresh)
}
