package com.newsapp.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.newsapp.common.R
import com.newsapp.common.base.BaseViewModel
import com.newsapp.common.helper.Event
import com.newsapp.home.domain.GetNewsSourcesUseCase
import com.newsapp.model.NewsSource
import com.newsapp.repository.AppDispatchers
import com.newsapp.repository.helper.Resource
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(
    private val getNewsSourcesUseCase: GetNewsSourcesUseCase,
    private val dispatchers: AppDispatchers
) : BaseViewModel() {

    val newsSourceMediator = MediatorLiveData<Resource<MutableList<NewsSource>>>()
    private var sources: LiveData<Resource<MutableList<NewsSource>>> = MutableLiveData()

    init {
        getNewsSources(false)
    }

    fun newsSourceClickHandler(newsSource: NewsSource) {
        // TODO: add navigation to article list screen
    }

    fun refreshListDataHandler() = getNewsSources(true)

    private fun getNewsSources(forceRefresh: Boolean) = viewModelScope.launch(dispatchers.main) {
        newsSourceMediator.removeSource(sources)

        withContext(dispatchers.io) {
            sources = getNewsSourcesUseCase(forceRefresh = forceRefresh)
        }

        newsSourceMediator.addSource(sources) {
            newsSourceMediator.value = it

            if (it.status == Resource.Status.ERROR) {
                snackBarError.value = Event(R.string.global_error_message)
            }
        }
    }
}
