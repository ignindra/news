package com.newsapp.home.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.newsapp.home.HomeViewModel
import com.newsapp.home.databinding.ItemHomeBinding
import com.newsapp.model.NewsSource

class HomeViewHolder(parent: View): RecyclerView.ViewHolder(parent) {

    private val binding = ItemHomeBinding.bind(parent)

    fun bindTo(newsSource: NewsSource, viewModel: HomeViewModel) {
        binding.newsSource = newsSource
        binding.viewmodel = viewModel
    }
}
