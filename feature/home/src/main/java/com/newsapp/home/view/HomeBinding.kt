package com.newsapp.home.view

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.newsapp.model.NewsSource
import com.newsapp.repository.helper.Resource

object HomeBinding {

    @BindingAdapter("app:showWhenLoading")
    @JvmStatic
    fun <T>showWhenLoading(view: SwipeRefreshLayout, resource: Resource<T>?) {
        if (resource != null) {
            view.isRefreshing = resource.status == Resource.Status.LOADING
        }
    }

    @BindingAdapter("app:items")
    @JvmStatic fun setItems(recyclerView: RecyclerView, resource: Resource<MutableList<NewsSource>>?) {
        with(recyclerView.adapter as HomeAdapter) {
            resource?.data?.let { updateData(it) }
        }
    }

    @BindingAdapter("app:showWhenEmptyList")
    @JvmStatic fun showMessageErrorWhenEmptyList(view: View, resource: Resource<MutableList<NewsSource>>?) {
        val isResourceValid = resource != null &&
                resource.status == Resource.Status.ERROR &&
                resource.data?.isEmpty() ?: false

        view.visibility = if (isResourceValid) View.VISIBLE else View.GONE
    }
}
