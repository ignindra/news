package com.newsapp.home.view

import androidx.recyclerview.widget.DiffUtil
import com.newsapp.model.NewsSource

class HomeItemDiffCallback(
    private val oldList: MutableList<NewsSource>,
    private val newList: MutableList<NewsSource>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int)
            = oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id &&
               oldList[oldItemPosition].name == newList[newItemPosition].name
    }
}
