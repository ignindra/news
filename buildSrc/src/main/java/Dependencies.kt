object Projects {
    val applicationId = "com.newsapp"
    val buildTools    = "29.0.2"
    val compileSdk    = 29
    val minSdk        = 19
    val targetSdk     = 29
    val versionCode   = 1
    val versionName   = "1.0"
}

object Versions {
    val appCompat        = "1.1.0"
    val constraintLayout = "1.1.3"
    val coreKtx          = "1.1.0"
    val coroutines       = "1.3.2"
    val glide            = "4.10.0"
    val gradle           = "3.5.1"
    val gson             = "2.8.6"
    val koin             = "2.0.1"
    val kotlin           = "1.3.50"
    val lifecycle        = "2.1.0"
    val navigation       = "2.1.0"
    val recyclerview     = "1.0.0"
    val retrofit         = "2.6.2"
    val room             = "2.1.0"
}

object Dependencies {
    val appCompat               = "androidx.appcompat:appcompat:${Versions.appCompat}"
    val constraintLayout        = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    val coreKtx                 = "androidx.core:core-ktx:${Versions.coreKtx}"
    val glide                   = "com.github.bumptech.glide:glide:${Versions.glide}"
    val gson                    = "com.google.code.gson:gson:${Versions.gson}"
    val koin                    = "org.koin:koin-android:${Versions.koin}"
    val koinViewModel           = "org.koin:koin-android-viewmodel:${Versions.koin}"
    val kotlinCoroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    val kotlinCoroutinesCore    = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
    val kotlinStandardLibrary   = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    val lifecycleExtensions     = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    val lifecycleViewModel      = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    val navigation              = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    val navigationFragment      = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    val recyclerView            = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"
    val retrofit                = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    val retrofitConverterGson   = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    val roomCompiler            = "androidx.room:room-compiler:${Versions.room}"
    val roomKtx                 = "androidx.room:room-ktx:${Versions.room}"
    val roomRunTime             = "androidx.room:room-runtime:${Versions.room}"
}

object InternalDependencies {
    val navigationModule = ":navigation"
    val commonModule     = ":common"
    val homeScreen       = ":feature:home"
    val localModule      = ":data:local"
    val modelModule      = ":data:model"
    val remoteModule     = ":data:remote"
    val repositoryModule = ":data:repository"
}
