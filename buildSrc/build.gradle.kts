import org.gradle.kotlin.dsl.`kotlin-dsl`

// Credit to https://proandroiddev.com/gradle-dependency-management-with-kotlin-94eed4df9a28

plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
}
