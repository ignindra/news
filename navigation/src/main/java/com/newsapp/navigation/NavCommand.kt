package com.newsapp.navigation

import androidx.navigation.NavDirections

sealed class NavCommand {
    data class To(val directions: NavDirections): NavCommand()
    object Back: NavCommand()
}
